<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'HIVE.HiveExtCountry',
            'Hiveextcountrycheckboxlist',
            'hive :: Country :: checkbox list'
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'HIVE.HiveExtCountry',
            'Hiveextcountryselectlist',
            'hive :: Country :: selectlistCountry'
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('hive_ext_country', 'Configuration/TypoScript', 'hive_ext_country');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hiveextcountry_domain_model_country', 'EXT:hive_ext_country/Resources/Private/Language/locallang_csh_tx_hiveextcountry_domain_model_country.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hiveextcountry_domain_model_country');

    }
);