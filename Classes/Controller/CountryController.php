<?php
namespace HIVE\HiveExtCountry\Controller;

/***
 *
 * This file is part of the "hive_ext_country" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Andreas Hafner <a.hafner@teufels.com>, teufels GmbH
 *           Dominik Hilser <d.hilser@teufels.com>, teufels GmbH
 *           Georg Kathan <g.kathan@teufels.com>, teufels GmbH
 *           Hendrik Krüger <h.krueger@teufels.com>, teufels GmbH
 *           Josymar Escalona Rodriguez <j.rodriguez@teufels.com>, teufels GmbH
 *           Perrin Ennen <p.ennen@teufels.com>, teufels GmbH
 *           Timo Bittner <t.bittner@teufels.com>, teufels GmbH
 *           Yannick Aister <y.aister@teufels.com>, teufels GmbH
 *
 ***/

/**
 * CountryController
 */
class CountryController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * countryRepository
     *
     * @var \HIVE\HiveExtCountry\Domain\Repository\CountryRepository
     * @inject
     */
    protected $countryRepository = null;

    /**
     * action checkboxList
     *
     * @return void
     */
    public function checkboxListAction()
    {

    }

    /**
     * action selectList
     *
     * @return void
     */
    public function selectListAction()
    {

    }
}
