<?php
namespace HIVE\HiveExtCountry\Domain\Model;

/***
 *
 * This file is part of the "hive_ext_country" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Andreas Hafner <a.hafner@teufels.com>, teufels GmbH
 *           Dominik Hilser <d.hilser@teufels.com>, teufels GmbH
 *           Georg Kathan <g.kathan@teufels.com>, teufels GmbH
 *           Hendrik Krüger <h.krueger@teufels.com>, teufels GmbH
 *           Josymar Escalona Rodriguez <j.rodriguez@teufels.com>, teufels GmbH
 *           Perrin Ennen <p.ennen@teufels.com>, teufels GmbH
 *           Timo Bittner <t.bittner@teufels.com>, teufels GmbH
 *           Yannick Aister <y.aister@teufels.com>, teufels GmbH
 *
 ***/

/**
 * Country
 */
class Country extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * title
     *
     * @var string
     * @validate NotEmpty
     */
    protected $title = '';

    /**
     * backendTitle
     *
     * @var string
     * @validate NotEmpty
     */
    protected $backendTitle = '';

    /**
     * belongsToDefaultSysLanguage
     *
     * @var bool
     * @validate NotEmpty
     */
    protected $belongsToDefaultSysLanguage = false;

    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the backendTitle
     *
     * @return string $backendTitle
     */
    public function getBackendTitle()
    {
        return $this->backendTitle;
    }

    /**
     * Sets the backendTitle
     *
     * @param string $backendTitle
     * @return void
     */
    public function setBackendTitle($backendTitle)
    {
        $this->backendTitle = $backendTitle;
    }

    /**
     * Returns the belongsToDefaultSysLanguage
     *
     * @return bool $belongsToDefaultSysLanguage
     */
    public function getBelongsToDefaultSysLanguage()
    {
        return $this->belongsToDefaultSysLanguage;
    }

    /**
     * Sets the belongsToDefaultSysLanguage
     *
     * @param bool $belongsToDefaultSysLanguage
     * @return void
     */
    public function setBelongsToDefaultSysLanguage($belongsToDefaultSysLanguage)
    {
        $this->belongsToDefaultSysLanguage = $belongsToDefaultSysLanguage;
    }

    /**
     * Returns the boolean state of belongsToDefaultSysLanguage
     *
     * @return bool
     */
    public function isBelongsToDefaultSysLanguage()
    {
        return $this->belongsToDefaultSysLanguage;
    }
}
