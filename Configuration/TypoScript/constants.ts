
plugin.tx_hiveextcountry_hiveextcountrycheckboxlist {
    view {
        # cat=plugin.tx_hiveextcountry_hiveextcountrycheckboxlist/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:hive_ext_country/Resources/Private/Templates/
        # cat=plugin.tx_hiveextcountry_hiveextcountrycheckboxlist/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:hive_ext_country/Resources/Private/Partials/
        # cat=plugin.tx_hiveextcountry_hiveextcountrycheckboxlist/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:hive_ext_country/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_hiveextcountry_hiveextcountrycheckboxlist//a; type=string; label=Default storage PID
        storagePid =
    }
}

plugin.tx_hiveextcountry_hiveextcountryselectlist {
    view {
        # cat=plugin.tx_hiveextcountry_hiveextcountryselectlist/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:hive_ext_country/Resources/Private/Templates/
        # cat=plugin.tx_hiveextcountry_hiveextcountryselectlist/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:hive_ext_country/Resources/Private/Partials/
        # cat=plugin.tx_hiveextcountry_hiveextcountryselectlist/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:hive_ext_country/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_hiveextcountry_hiveextcountryselectlist//a; type=string; label=Default storage PID
        storagePid =
    }
}


## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

plugin {
    tx_hiveextcountry {

        persistence {
            storagePid =
        }

        model {
            HIVE\HiveExtCountry\Domain\Model\Country {
                persistence {
                    storagePid =
                }
            }
            HIVE\HiveExtCountry\Domain\Model\Abstract {
                persistence {
                    storagePid =
                }
            }
        }
    }
}